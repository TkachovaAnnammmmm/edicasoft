import React, { useState } from "react";

import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Formik } from "formik";

import * as Yup from "yup";

function UserProfile(props) {
  const {
    userProfile,
    handleDetailsFormSubmit,
    handleDetailsFormCancel,
    disabled,
  } = props;

  const [isSubmitionCompleted, setSubmitionCompleted] = useState(false);

  return (
    !isSubmitionCompleted && (
      <Formik
        key={userProfile.id || "nokey"}
        initialValues={{
          name: userProfile.name || "",
          username: userProfile.username || "",
          email: userProfile.email || "",
          street: (userProfile.address && userProfile.address.street) || "",
          suite: (userProfile.address && userProfile.address.suite) || "",
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);

          setTimeout(() => {
            handleDetailsFormSubmit({
              ...values,
              address: { street: values.street, suite: values.suite },
            });
            setSubmitionCompleted(true);
          }, 400);
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string(),
          username: Yup.string().required(),
          email: Yup.string().email().required(),
          street: Yup.string(),
          suite: Yup.string(),
        })}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;
          return (
            <form onSubmit={handleSubmit}>
              <TextField
                label="Name"
                name="name"
                variant="outlined"
                disabled={disabled}
                fullWidth
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={errors.name && touched.name && errors.name}
                margin="normal"
              />

              <TextField
                label="User Name"
                name="username"
                variant="outlined"
                disabled={disabled}
                fullWidth
                value={values.username}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={
                  errors.username && touched.username && errors.username
                }
                margin="normal"
              />
              <TextField
                label="Email"
                name="email"
                variant="outlined"
                disabled={disabled}
                fullWidth
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={errors.email && touched.email && errors.email}
                margin="normal"
              />
              <Typography variant="h6">Address</Typography>

              <TextField
                label="Street"
                name="street"
                variant="outlined"
                disabled={disabled}
                fullWidth
                value={values.street}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={errors.street && touched.street && errors.street}
                margin="normal"
              />
              <TextField
                label="Suite"
                name="suite"
                variant="outlined"
                disabled={disabled}
                fullWidth
                value={values.suite}
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={errors.suite && touched.suite && errors.suite}
                margin="normal"
              />

              {!disabled && (
                <div>
                  <Button
                    type="button"
                    className="outline"
                    onClick={handleDetailsFormCancel}
                    disabled={isSubmitting}
                  >
                    Cancel
                  </Button>
                  <Button type="submit" disabled={isSubmitting}>
                    Save
                  </Button>
                </div>
              )}
            </form>
          );
        }}
      </Formik>
    )
  );
}

export default UserProfile;
