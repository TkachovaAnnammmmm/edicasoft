import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";

import UserProfile from "../userProfile/UserProfile";
import SortByName from "../../components/SortByName";
import UsersListRow from "../../components/UsersListRow";
import Loading from "../../components/Loading";

function UsersList(props) {
  const dispatch = useDispatch();
  const [sortedList, setSortedList] = useState([]);

  const {
    usersList,
    loadingUsersList,
    errorUsersList,
    currentUser,
    isDisabled,
    isNew,
    processingUserCreate,
    processingUserUpdate,
    processingUserDelete,
  } = useSelector((state) => ({
    usersList: state.usersList.usersList,
    loadingUsersList: state.usersList.loadingUsersList,
    errorUsersList: state.usersList.errorUsersList,
    currentUser: state.userProfile.currentUser,
    isDisabled: state.userProfile.isDisabled,
    isNew: state.userProfile.isNew,
    processingUserCreate: state.userProfile.processingUserCreate,
    processingUserUpdate: state.userProfile.processingUserUpdate,
    processingUserDelete: state.userProfile.processingUserDelete,
  }));

  useEffect(() => {
    let didCancel = false;
    if (!didCancel) {
      dispatch({
        type: "GET_USERS_LIST",
      });
    }
    return () => {
      didCancel = true;
    };
  }, [dispatch]);

  useEffect(() => {
    let didCancel = false;
    if (!didCancel && usersList.length) {
      setSortedList(usersList);
    }
    return () => {
      didCancel = true;
    };
  }, [usersList]);

  const handleDelete = (id) => {
    // if (processingUserCreate || processingUserUpdate || processingUserDelete) {
    //   return;
    // }
    dispatch({
      type: "DELETE_USER",
      id: id,
    });
  };

  const handleChangeName = (ev) => {
    const newArr = usersList.filter(
      (user) =>
        user.name.toLowerCase().includes(ev.target.value.toLowerCase()) && user
    );
    setSortedList(newArr);
  };

  const handleUpdate = (user) => {
    // if (processingUserCreate || processingUserUpdate || processingUserDelete) {
    //   return;
    // }
    dispatch({
      type: "SET_CURRENT_USER",
      user: user,
      isDisabled: false,
      isNew: false,
    });
  };

  const handleRowClick = (user) => {
    if (processingUserCreate || processingUserUpdate || processingUserDelete) {
      return;
    }
    dispatch({
      type: "SET_CURRENT_USER",
      user: user,
      isDisabled: true,
      isNew: false,
    });
  };

  const handleAddNewUser = () => {
    dispatch({
      type: "SET_CURRENT_USER",
      user: {},
      isDisabled: false,
      isNew: true,
    });
  };

  const handleDetailsFormCancel = () => {
    dispatch({
      type: "SET_CURRENT_USER",
      user: null,
      isDisabled: true,
      isNew: false,
    });
  };

  const handleDetailsFormSubmit = (data) => {
    if (!isNew) {
      dispatch({
        type: "UPDATE_USER",
        id: currentUser.id,
        data: data,
      });
    } else {
      dispatch({
        type: "CREATE_NEW_USER",
        user: data,
      });
    }
  };

  return loadingUsersList ? (
    <Loading />
  ) : errorUsersList ? (
    <div>error</div>
  ) : (
    <Grid container spacing={0}>
      <Grid item xs={12} lg={6}>
        <Typography variant="h6">Users List</Typography>
        <SortByName handleChangeName={handleChangeName} />
        <List>
          {sortedList.length
            ? sortedList.map((user) => (
                <UsersListRow
                  key={user.id}
                  user={user}
                  handleRowClick={handleRowClick}
                  handleDelete={handleDelete}
                  handleUpdate={handleUpdate}
                  disableButtons={processingUserCreate || processingUserUpdate || processingUserDelete}
                ></UsersListRow>
              ))
            : null}
        </List>
      </Grid>

      <Grid item xs={12} lg={6}>
        <Button onClick={handleAddNewUser} disabled={processingUserCreate || processingUserUpdate || processingUserDelete}>Add New User</Button>
        {(processingUserCreate || processingUserUpdate || processingUserDelete)&& <Typography variant="h6">Processing.....</Typography>}
        {currentUser && (
          <UserProfile
            userProfile={currentUser}
            disabled={isDisabled}
            handleDetailsFormCancel={handleDetailsFormCancel}
            handleDetailsFormSubmit={handleDetailsFormSubmit}
          />
        )}
      </Grid>
    </Grid>
  );
}

export default UsersList;
