import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import history from "../history";

import UsersList from "./usersList/UsersList";

function HOC() {

   return  (
    <Router history={history}>
      <div className="App">
        <Switch>
          <Route path={"/"} component={UsersList} />
          <Redirect  to="/"/>
        </Switch>
      </div>
    </Router>
  );
}

export default HOC;