import React from "react";
import { CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
    alignSelf: "center"
  }
}));

function Loading() {
  const classes = useStyles();
  return <CircularProgress size={68} className={classes.progress} />;
}

export default Loading;
