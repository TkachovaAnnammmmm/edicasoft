import React from "react";

import TextField from "@material-ui/core/TextField";

function SortByName(props) {
  const { handleChangeName } = props;
  return (
    <form noValidate>
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        name="search"
        label="Search By Name"
        type="text"
        id="text"
        autoCorrect="off"
        autoCapitalize="none"
        autoComplete=""
        onChange={handleChangeName}
      />
    </form>
  );
}

export default SortByName;
