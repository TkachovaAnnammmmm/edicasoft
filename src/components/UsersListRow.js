import React from "react";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import Delete from "@material-ui/icons/Delete";
import Create from "@material-ui/icons/Create";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  rowUser: {
    cursor: "pointer",
    borderBottom: "solid 1px #eee",
  },
}));

function UsersListRow(props) {
  const {
    user,
    handleRowClick,
    handleUpdate,
    handleDelete,
    disableButtons,
  } = props;
  const classes = useStyles();

  return (
    <ListItem onClick={() => handleRowClick(user)} className={classes.rowUser}>
      <ListItemText>
        {user.name} {user.id}
      </ListItemText>
      <Button
        disabled={disableButtons}
        onClick={(ev) => {
          ev.stopPropagation();
          handleUpdate(user);
        }}
      >
        <Create />
        Update
      </Button>
      <Button
        disabled={disableButtons}
        onClick={(ev) => {
          ev.stopPropagation();
          handleDelete(user.id);
        }}
      >
        <Delete />
        Delete
      </Button>
    </ListItem>
  );
}

export default UsersListRow;
