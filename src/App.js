import React from "react";

import { Provider } from "react-redux";

import { MuiThemeProvider } from "@material-ui/core/styles";
import { useTheme } from "@material-ui/core/styles";

import "./App.css";
import store from "./store";
import HOC from "./containers/HOC";
import CssBaseline from "@material-ui/core/CssBaseline";

function App({ ...children }) {
  const theme = useTheme();
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <HOC>{children}</HOC>
      </MuiThemeProvider>
    </Provider>
  );
}

export default App;
