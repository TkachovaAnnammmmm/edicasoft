import { put, call } from "redux-saga/effects";

export function deleteUserAPI(action) {
  return fetch(`https://jsonplaceholder.typicode.com/users/${action.id}`, {
    method: "DELETE",
  })
    .then((response) => {
      response.json();
    })
    .then((json) => ({ response: json }))
    .catch((error) => ({ error }));
}

export function* deleteUser(action) {
  const { response, error } = yield call(() => deleteUserAPI(action));

  if (response) {
    //there is no response...just 200 status
    //    console.log(response);
    yield put({
      type: "USER_DELETE_SUCCESS",
    });
    yield put({
      type:"GET_USERS_LIST",
    })
  } else {
    yield put({
      type: "USER_DELETE_ERROR",
      message: "Error happen",
    });
  }
}

export function createUserAPI(action) {
  return fetch("https://jsonplaceholder.typicode.com/users", {
    method: "POST",
    body: JSON.stringify(action.data),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
      "Access-Control-Allow-Headers":
        "Origin, X-Requested-With, Content-Type, Accept, Cache-Control",
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((json) => ({ response: json }))
    .catch((error) => ({ error }));
}

export function* createUser(action) {
  const { response, error } = yield call(() => createUserAPI(action));

  if (response) {
    // console.log(response);
    yield put({
      type: "CREATE_NEW_USER_SUCCESS",
    });
    yield put({
      type:"GET_USERS_LIST",
    })
  } else {
    yield put({
      type: "CREATE_NEW_USER_ERROR",
      message: "Error happen",
    });
  }
}

export function updateUserAPI(action) {
  return fetch(`https://jsonplaceholder.typicode.com/users/${action.id}`, {
    method: "PUT",
    body: JSON.stringify(action.data),
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE",
      "Access-Control-Allow-Headers":
        "Origin, X-Requested-With, Content-Type, Accept, Cache-Control",
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((json) => ({ response: json }))
    .catch((error) => ({ error }));
}

export function* updateUser(action) {
  const { response, error } = yield call(() => updateUserAPI(action));
  if (response) {
    // console.log(response);
    yield put({
      type: "UPDATE_USER_SUCCESS",
    });
    yield put({
      type:"GET_USERS_LIST",
    })
  } else {
    yield put({
      type: "UPDATE_USER_ERROR",
      message: "Error happen",
    });
  }
}
