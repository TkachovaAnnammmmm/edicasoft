import { put, call } from "redux-saga/effects";

export function fetchUsersListAPI(action) {
  return fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((json) => ({ response: json }))
    .catch((error) => ({ error }));
}

export function* fetchUsersList(action) {
  const { response, error } = yield call(() => fetchUsersListAPI(action));

  if (response) {
    yield put({
      type: "UPDATE_USERS_LIST",
      usersList: response,
    });
    yield put({
      type: "USERS_LIST_SUCCESS",
    });
  } else {
    yield put({
      type: "USERS_LIST_ERROR",
      message: "Error happen",
    });
  }
}
