import { takeLatest, all } from "redux-saga/effects";

import { deleteUser, createUser, updateUser } from "./userProfile";
import { fetchUsersList } from "./usersList";

function* actionWatcher() {
  yield takeLatest("DELETE_USER", deleteUser);
  yield takeLatest("CREATE_NEW_USER", createUser);
  yield takeLatest("UPDATE_USER", updateUser);
  yield takeLatest("GET_USERS_LIST", fetchUsersList);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
