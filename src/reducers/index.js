import { combineReducers } from "redux";
import userProfile from "./userProfile";
import usersList from "./usersList";

export default combineReducers({
    userProfile,
    usersList
});