const initialState = {
    loadingUsersList: true,
    usersList: [],
    errorUsersList: "",
  };
  
  const usersList = (state = initialState, action) => {
    switch (action.type) {
      case "GET_USERS_LIST":
        return { ...state, loadingUsersList: true, errorUsersList: "" };
      case "UPDATE_USERS_LIST":
        return {
          ...state,
          usersList: action.usersList
        };
      case "USERS_LIST_SUCCESS":
        return {
          ...state,
          loadingUsersList: false,
          errorUsersList: ""
        };
      case "USERS_LIST_ERROR":
        return {
          ...state,
          loadingUsersList: false,
          errorUsersList: action.message
        };
      default:
        return state;
    }
  };
  
  export default usersList;