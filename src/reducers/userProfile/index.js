const initialState = {
  currentUser: null,
  isDisabled: true,
  isNew: false,
  processingUserDelete: false,
  errorUserDelete: "",
  processingUserUpdate: false,
  errorUserUpdate: "",
  processingUserCreate: false,
  errorUserCreate: "",
};

const usersList = (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_USER":
      return {
        ...state,
        processingUserDelete: true,
        errorUserDelete: "",
      };

    case "USER_DELETE_SUCCESS":
      return {
        ...state,
        processingUserDelete: false,
        errorUserDelete: "",
      };
    case "USER_DELETE_ERROR":
      return {
        ...state,
        processingUserDelete: false,
        errorUserDelete: action.message,
      };
    case "UPDATE_USER":
      return {
        ...state,
        processingUserUpdate: true,
        errorUserUpdate: "",
      };
    case "UPDATE_USER_SUCCESS":
      return {
        ...state,
        currentUser: null,
        isDisabled: true,
        isNew: false,
        processingUserUpdate: false,
        errorUserUpdate: "",
      };
    case "UPDATE_USER_ERROR":
      return {
        ...state,
        currentUser: null,
        isDisabled: true,
        isNew: false,
        processingUserUpdate: false,
        errorUserUpdate: action.message,
      };
    case "CREATE_NEW_USER":
      return {
        ...state,
        processingUserCreate: true,
        errorUserCreate: "",
      };
    case "CREATE_NEW_USER_SUCCESS":
      return {
        ...state,
         currentUser: null,
        isDisabled: true,
        isNew: false,
        processingUserCreate: false,
        errorUserCreate: "",
      };
    case "CREATE_NEW_USER_ERROR":
      return {
        ...state,
        currentUser: null,
        isDisabled: true,
        isNew: false,
        processingUserCreate: false,
        errorUserCreate: action.message,
      };
    case "SET_CURRENT_USER":
      return {
        ...state,
        currentUser: action.user,
        isDisabled: action.isDisabled,
        isNew: action.isNew,
      };
    default:
      return state;
  }
};

export default usersList;
