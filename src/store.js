import createSagaMiddleware from "redux-saga";
import { compose, createStore, applyMiddleware } from "redux";

import { logger } from "redux-logger";

import reducer from "./reducers";
import rootSaga from "./sagas";
import history from "./history";
import { routerMiddleware } from "react-router-redux";


const sagaMiddleware = createSagaMiddleware();

let middleware = [sagaMiddleware, routerMiddleware(history)];

if (process.env.NODE_ENV === `development`) {
  middleware.push(logger);
}
const store = compose(applyMiddleware(...middleware))(createStore)(reducer);
sagaMiddleware.run(rootSaga);
export default store;